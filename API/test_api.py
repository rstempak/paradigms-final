import unittest
import requests
import json
from _nba_database import _nba_database


class Testnba(unittest.TestCase):
    db = _nba_database()
    db.load_players('../data/Players.csv')

    def reset_data(self):
        self.db.load_players('../data/Players.csv')

    def test_get_player(self):
        resp = self.db.get_player("Klay Thompson")

        self.assertEqual(resp['height'], '201')
        self.assertEqual(resp['weight'], '97')
        self.assertEqual(resp['college'], 'Washington State University')

    def test_set_player(self):
        self.reset_data()
        name = "Nick Ribera"
        data = {"name": "Nick Ribera", "id": "3922", "height": "200" , "weight": "95", "college": "University of Notre Dame" , "born": "1996", "birth_city": "San Francisco", "birth_state": "California"}
        self.db.set_player(name, data)

        # call load_players
        resp = self.db.get_player("Nick Ribera")

        self.assertEqual(resp['name'], 'Nick Ribera')
        self.assertEqual(resp['height'], '200')
        self.assertEqual(resp['weight'], '95')
        self.assertEqual(resp['college'], 'University of Notre Dame')
        self.assertEqual(resp['id'], '3922')
        self.assertEqual(resp['born'], '1996')

    def test_get_max(self):
        # test height
        resp = self.db.get_max_by_attribute("height")

        self.assertEqual(resp['name'], 'Gheorghe Muresan')
        self.assertEqual(resp['height'], '231')

        # test weight
        resp = self.db.get_max_by_attribute("weight")

        self.assertEqual(resp['name'], 'Sim Bhullar')
        self.assertEqual(resp['weight'], '163')

    def test_get_min(self):
        # test height
        resp = self.db.get_min_by_attribute("height")

        self.assertEqual(resp['name'], 'Muggsy Bogues')
        self.assertEqual(resp['height'], '160')

        # test weight
        resp = self.db.get_min_by_attribute("weight")

        self.assertEqual(resp['name'], 'Spud Webb')
        self.assertEqual(resp['weight'], '60')
if __name__ == "__main__":
    unittest.main()
