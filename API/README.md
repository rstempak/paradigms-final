Paul Githens Nick Ribera RJ Stempak
26 November 2017
Programming Paradigms
OO API Readme

Our API reads in the data from our file on the nba data and puts it 
into a dictionary with the player name being the key and value of the data 
itself. Our api can be used to get a read out of all players data, a specific 
players data, it can alo add a player and player data to the dictionary as well 
as delete a player from the dictionary. The api can also list all players with 
a common attribute, such as all players who went to a specific school, or all 
players of a specific height. Our api can then take this list of players and 
randomly generate a team from the players with this common attribute, assuming 
there are at least 5 players with this attribute. The api can then also get the 
player with either the highest value or lowest value for certain attributes, 
such as height, weight, date of birth, and so on. Overall, the api allows the 
user to organize and view data in an easy to read manner. 