# RJ STEMPAK PAUL GITHENS NICK RIBERA 26 NOV 2017
# PROGRAMMING PARADIGMS
# _nba_database class
import numpy as np
from random import *
import re
import csv

class _nba_database:

	def __init__(self):
		self.players = {}

	def load_players(self, source):
		self.delete_all_players()
		# id,player,height,weight,college,born,birth_city,birth_state
		with open(source, 'r') as f:
			reader = csv.reader(f)
			for row in reader:
				pid,player,height,weight,college,born,birth_city,birth_state  = row
				self.players[player] = {"name": player, "height": height, "id" : pid,
										"weight": weight, "college": college, "birthdate": born,
										"hometown": birth_city, "homestate": birth_state}

	def get_player(self, name):
		if name in self.players:
			return self.players[name]
		else:
			return None

	def get_players(self):
		return [name for name in self.players]

	def set_player(self, name, data):
		self.players[name] = data

	def delete_player(self, name):
		self.players.pop(name, None)

	def get_player_by_attribute(self, attribute, value):
		match_players = []
		for name in self.players:
			if self.players[name][attribute] == value:
				match_players.append(self.players[name])
		return match_players

	def get_team_from_list(self, lst):
		team_lst = []
		while len(team_lst) <= 5 and len(lst) > 0:
			x = randint(0, len(lst) - 1)
			team_lst.append(lst[x])
			lst.pop(x)
		return team_lst

	def get_max_by_attribute(self, attribute):
		if self.players:
			max_val = 0
			max_player = None
			for name in sorted(self.players):
				if self.players[name][attribute] and int(self.players[name][attribute]) > max_val:
					max_val = int(self.players[name][attribute])
					max_player = self.players[name]
			return max_player
		else:
			return None

	def get_min_by_attribute(self, attribute):
		if self.players:
			min_val = 1000000
			min_player = None
			for name in sorted(self.players):
				if self.players[name][attribute] and int(self.players[name][attribute]) < min_val:
					min_val = int(self.players[name][attribute])
					min_player =  self.players[name]
			return min_player
		else:
			return None

	def delete_all_players(self):
		for player in self.players:
			self.players[player].clear()
		self.players.clear()
