# RJ STEMPAK 11 OCT 2017
# PROGRAMMING PARADIGMS

import cherrypy
from pcont import PlayerController
from ocont import OptionsController
import sys
sys.path.insert(0, '../API/')
from _nba_database import _nba_database

def CORS():
	cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
	cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE"
	cherrypy.response.headers["Access-Control-Allow-Credentials"] = "*"

def start_service():
	nbadb = _nba_database()
	playerController = PlayerController(nbadb = nbadb)
	optionsController = OptionsController()
	dispatcher = cherrypy.dispatch.RoutesDispatcher()

	conf = { 'global' : {'server.socket_host': 'student04.cse.nd.edu','server.socket_port': 51066}, '/' : {'request.dispatch': dispatcher, 'tools.CORS.on': True} }

	#################### RESET ####################
	#/reset/ CONNECTIONS
	dispatcher.connect('reset', '/reset/', controller=playerController, action = 'RESET', conditions=dict(method=['PUT']))
	dispatcher.connect('reset_options', route='/reset/', controller=optionsController,action='OPTIONS', conditions=dict(method=['OPTIONS']))

	#################### PLAYER CONNECTIONS ###################
	#/players/
	dispatcher.connect('get_players', '/players/', controller=playerController, action = 'GET', conditions=dict(method=['GET']))
	dispatcher.connect('post_players', '/players/', controller=playerController, action = 'POST', conditions=dict(method=['POST']))
	dispatcher.connect('players_options', route='/players/', controller=optionsController,action='OPTIONS', conditions=dict(method=['OPTIONS']))

	#/players/:name
	dispatcher.connect('get_single', '/players/:name', controller=playerController, action = 'GET_SINGLE', conditions=dict(method=['GET']))
	dispatcher.connect('delete_single', '/players/:name', controller=playerController, action = 'DELETE_SINGLE', conditions=dict(method=['DELETE']))
	dispatcher.connect('player_s_options', route='/players/:name', controller=optionsController,action='OPTIONS', conditions=dict(method=['OPTIONS']))

	#/min/:attribute
	#/max/:attribute
	dispatcher.connect('get_min', '/min/:attribute', controller=playerController, action = 'GET_MIN', conditions=dict(method=['GET']))
	dispatcher.connect('get_max', '/max/:attribute', controller=playerController, action = 'GET_MAX', conditions=dict(method=['GET']))
	dispatcher.connect('min_options', route='/min/:attribute', controller=optionsController,action='OPTIONS', conditions=dict(method=['OPTIONS']))
	dispatcher.connect('max_options', route='/max/:attribute', controller=optionsController,action='OPTIONS', conditions=dict(method=['OPTIONS']))

	#/team/
	dispatcher.connect('get_team', '/team/', controller=playerController, action = 'PUT_TEAM', conditions=dict(method=['PUT']))
	dispatcher.connect('team_options', route='/team/', controller=optionsController,action='OPTIONS', conditions=dict(method=['OPTIONS']))

	cherrypy.config.update(conf)
	app = cherrypy.tree.mount(None, config=conf)
	cherrypy.quickstart(app)

if __name__ == '__main__':
	cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
	start_service()
