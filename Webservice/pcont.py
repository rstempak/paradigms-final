# RJ STEMPAK 11 OCT 2017
# PROGRAMMING PARADIGMS
# mcont class

import cherrypy
import re, json

class PlayerController(object):
    def __init__(self, nbadb = None):
        self.nbadb = nbadb
        self.nbadb.load_players('../data/Players.csv')

    ######### /players/ ############
    def GET(self):
        output = {}
        output['players'] = []
        for p in self.nbadb.players:
            player = self.nbadb.players[p]
            player["result"] = "success"

            output['players'].append(player)

        output['result'] = 'success'
        return json.dumps(output)

    def POST(self):
        #the read will get you the body and the decode will make sure it is a string
        payload = cherrypy.request.body.read().decode()
        addition = json.loads(payload)
        output = {"result": "success"}

        #try to add player to database
        try:
            self.nbadb.players[addition["name"]] = addition
        except KeyError as ex:
            output['result'] = 'error'

        return json.dumps(output)

    ####### /players/:name ###########
    def GET_SINGLE(self, name):
        player = self.nbadb.get_player(name.replace("%20", " "))

        if not player:
            player = {"result":"error"}
        else:
            player["result"] = "success"
        return json.dumps(player)

    def DELETE_SINGLE(self, name):
        output = {'result':'success'}
        try:
            self.nbadb.delete_player(name.replace("%20", " "))
        except KeyError as ex:
            output['result'] = 'error'
            output['message'] = 'player not found'

        return json.dumps(output)

    ####### /min/:attribute ###########
    def GET_MIN(self, attribute):
        min_player = self.nbadb.get_min_by_attribute(attribute)

        if min_player:
            output = {"result" : "success", "player": min_player}
        else:
            output = {"result": "error"}

        return json.dumps(output)

    ####### /max/:attribute ###########
    def GET_MAX(self, attribute):
        max_player = self.nbadb.get_max_by_attribute(attribute)

        if max_player:
            output = {"result" : "success", "player": max_player}
        else:
            output = {"result": "error"}

        return json.dumps(output)

    ####### /team/ ###########
    def PUT_TEAM(self):
        #the read will get you the body and the decode will make sure it is a string
        payload = cherrypy.request.body.read().decode()
        data = json.loads(payload)
        potential_team = self.nbadb.get_player_by_attribute(data["attribute"],data["value"])
        final_team = self.nbadb.get_team_from_list(potential_team)
        if final_team:
            output = {"result": "success", "team": final_team}
        else:
            output = {"result": "error"}

        return json.dumps(output)

    ######### /reset/ ############
    def RESET(self):
        output = {'result':'success'}
        self.nbadb.load_players('../data/Players.csv')
        return json.dumps(output)
