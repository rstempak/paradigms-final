import unittest
import requests
import json


class Testnba(unittest.TestCase):
    PORT_NUM = '51066'
    print("Testing Port number: ", PORT_NUM)
    SITE_URL = 'http://student04.cse.nd.edu:' + PORT_NUM
    PLAYER_URL = SITE_URL + '/players/'
    RESET_URL = SITE_URL + '/reset/'
    TEAM_URL = SITE_URL + '/team/'
    MAX_URL = SITE_URL + '/max/'
    MIN_URL = SITE_URL + '/min/'

    def reset_data(self):
        m = {}
        r = requests.put(self.RESET_URL)

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_get_player(self):
        player_name = 'Klay%20Thompson'

        # request with player name get player
        r = requests.get(self.PLAYER_URL + player_name)
        resp = json.loads(r.content)
        self.assertEqual(resp['height'], '201')
        self.assertEqual(resp['weight'], '97')
        self.assertEqual(resp['college'], 'Washington State University')

    def test_set_player(self):
        self.reset_data()
        name = "Nick Ribera"
        data = {"name": "Nick Ribera", "id": "3922", "height": "200" , "weight": "95", "college": "University of Notre Dame" , "born": "1996", "birth_city": "San Francisco", "birth_state": "California"}

	# post player data test
        r = requests.post(self.PLAYER_URL, json = data)
        resp = json.loads(r.content)
        self.assertEqual(resp['result'], 'success')
	# request get test
        r = requests.get(self.PLAYER_URL + 'Nick%20Ribera')
        resp = json.loads(r.content)
        self.assertEqual(resp['name'], 'Nick Ribera')
        self.assertEqual(resp['height'], '200')
        self.assertEqual(resp['weight'], '95')
        self.assertEqual(resp['college'], 'University of Notre Dame')
        self.assertEqual(resp['id'], '3922')
        self.assertEqual(resp['born'], '1996')

    def test_get_max(self):
	# test get max by height attribute
        attribute = 'height'

        r = requests.get(self.MAX_URL + attribute)
        resp = json.loads(r.content)
        self.assertEqual(resp['player']['name'], 'Gheorghe Muresan')
        self.assertEqual(resp['player']['height'], '231')

	# # test get max by weight attribute
        attribute = 'weight'

        r = requests.get(self.MAX_URL + attribute)
        resp = json.loads(r.content)
        self.assertEqual(resp['player']['name'], 'Sim Bhullar')
        self.assertEqual(resp['player']['weight'], '163')

    def test_get_min(self):
	# # test get min by height attribute
        attribute = 'height'

        r = requests.get(self.MIN_URL + attribute)
        resp = json.loads(r.content)
        self.assertEqual(resp['player']['name'], 'Muggsy Bogues')
        self.assertEqual(resp['player']['height'], '160')

	# # test get min by weight attribute
        attribute = 'weight'

        r = requests.get(self.MIN_URL + attribute)
        resp = json.loads(r.content)
        self.assertEqual(resp['player']['name'], 'Spud Webb')
        self.assertEqual(resp['player']['weight'], '60')

    def test_delete(self):
        self.reset_data()
        player_name = 'Brandon%20Knight'

	# test delete player
        m = {}
        r = requests.delete(self.PLAYER_URL + player_name)
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.PLAYER_URL + player_name)
        resp = json.loads(r.content)
        self.assertEqual(resp['result'], 'error')


    def test_team(self):
        self.reset_data()
        data = {'attribute': 'college', 'value' : 'University of Kentucky'}

	# put check team
        r = requests.put(self.TEAM_URL, json = data)
        resp = json.loads(r.content)
        self.assertEqual(resp['result'], 'success')

if __name__ == "__main__":
    unittest.main()
