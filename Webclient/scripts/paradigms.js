
function Item() {
	this.addToDocument= function(parentdiv){
		parentdiv.appendChild(this.item)
		document.body.appendChild(parentdiv)
	}
}

function Label() {
	this.createLabel= function(text, id){
		this.item = document.createElement("p");
		this.item.setAttribute("id", id);
		var textLabel = document.createTextNode(text);
		this.item.appendChild(textLabel);
	},
	this.setText= function(text){
		this.item.innerHTML = text;
	}
}


function Button() {
	this.createButton= function(text, id){
		this.item = document.createElement("button");
		this.item.setAttribute("id", id);
		var textLabel = document.createTextNode(text);
		this.item.appendChild(textLabel);
	},
	this.addClickEventHandler= function(handler, args){
		this.item.onmouseup = function(){
			handler(args);
		};
	}
}

function Dropdown() {
	this.createDropdown= function(name, labels, id){
		this.item = document.createElement("select");
		this.item.setAttribute("id", id);
		this.name = name

		for ( var x in labels){
			//alert(x)
			//alert(labels[x])
			var lx = labels[x]
			var tmp = document.createElement("option")
			tmp.setAttribute("value", lx);
			tmp.setAttribute("name", name);
			var t = document.createTextNode(lx);
			tmp.appendChild(t)
			this.item.appendChild(tmp)
		}
	}

}
