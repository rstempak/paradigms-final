Label.prototype = new Item;
Button.prototype = new Item;
Image.prototype = new Item;
Dropdown.prototype = new Item;

var xhp = new XMLHttpRequest()
xhp.open("PUT", "http://student04.cse.nd.edu:51066/reset/", true)
xhp.send(JSON.stringify({}))



var url = "http://student04.cse.nd.edu:51066/"

parentdiv = document.createElement("div");
parentdiv.setAttribute("id", "parentDiv");

title = new Label()
title.createLabel("Player's Database", "titleLabel");
title.addToDocument(parentdiv);

attributeDropdown = new Dropdown()
var attributes = ["name", "height", "id", "weight", "college", "birthdate", "hometown", "homestate"]
attributeDropdown.createDropdown("dropAtt", attributes, "dropAttSel");
attributeDropdown.addToDocument(parentdiv)

GENbutton = new Button()
GENbutton.createButton("Generate Team", "generateButton");
GENbutton.addToDocument(parentdiv)
var args = [player1, player2, player3, player4, player5];
GENbutton.addClickEventHandler(generateTeam, args);

MAXbutton = new Button()
MAXbutton.createButton("Maximum of Attriibute", "maxButton");
MAXbutton.addToDocument(parentdiv)
var args = ["max/", player1, player2, player3, player4, player5];
MAXbutton.addClickEventHandler(MinMax, args);

MINbutton = new Button()
MINbutton.createButton("Minimum of Attribute", "minButton");
MINbutton.addToDocument(parentdiv)
var args = ["min/", player1, player2, player3, player4, player5];
MINbutton.addClickEventHandler(MinMax, args);

player1 = new Label()
player1.createLabel("", "player1Label");
player1.addToDocument(parentdiv);

player2 = new Label()
player2.createLabel("", "player2Label");
player2.addToDocument(parentdiv);

player3 = new Label()
player3.createLabel("", "player3Label");
player3.addToDocument(parentdiv);

player4 = new Label()
player4.createLabel("", "player4Label");
player4.addToDocument(parentdiv);

player5 = new Label()
player5.createLabel("", "player5Label");
player5.addToDocument(parentdiv);

function MinMax(args){
        var xhl = new XMLHttpRequest()
        var movieurl = url + args[0]
        xhl.open("GET", movieurl, true)
        xhl.onload = function() {
                if (xhl.readyState == 4 && xhl.status == 200) {
                        var temp = JSON.parse(xhl.responseText);
                        var temp_player = temp[player]
                        for (var i = 0; i < 5; i++){
                                args[i].setText("")
                        }
                        args[1].setText("Player: ")
                        args[2].setText( "Name: " + temp_player["name"] + "\nBorn: " + temp_player["hometown"] + ", " + temp_player["homestate"] + "\nDate of Birth: " + temp_player["birthdate"] + "\nCollege: " + temp_player["college"] + "\nHeight and Weight: " + temp_player["height"] + ", " + temp_player["weight"]);
                }
        }
        xhl.onerror = function(e){
                error( xhl.statusText );
        }
        xhl.send(null)
}

function generateTeam(args){
        var xhl = new XMLHttpRequest()
        var movieurl = url + "team/"
        xhl.open("PUT", movieurl, true)
        xhl.onload = function() {
                if (xhl.readyState == 4 && xhl.status == 200) {
                        var temp = JSON.parse(xhl.responseText);
                        var temp_team = temp[team]
                        for (var i = 0; i < 5; i++){
                                args[i].setText( "Name: " + temp_team[i]["name"] + "\nBorn: " + temp_team[i]["hometown"] + ", " + temp_team[i]["homestate"] + "\nDate of Birth: " + temp_team[i]["birthdate"] + "\nCollege: " + temp_team[i]["college"] + "\nHeight and Weight: " + temp_team[i]["height"] + ", " + temp_team[i]["weight"]);
                        }
                }
        }
        xhl.onerror = function(e){
                error( xhl.statusText );
        }
        var person = prompt("Please Enter An Attribute", "University of Notre Dame");
        if (person != null) {
                xhl.send(JSON.stringify({"attribute":attributeDropdown.value, "value":person}))
        }
        else {
                xhl.send(JSON.stringify({"attribute":attributeDropdown.value, "value":"University of Notre Dame"}))
        }
}



